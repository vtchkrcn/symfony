<?php

use App\Kernel;

require_once dirname(__DIR__).'/vendor/autoload_runtime.php';

\Sentry\init(['dsn' => 'https://f503546cb5174ab0aa7f7edb973cb7f3@o4504923728445440.ingest.sentry.io/4504923753611264']);

return function (array $context) {
    return new Kernel($context['APP_ENV'], (bool) $context['APP_DEBUG']);
};
