<?php

declare(strict_types=1);

namespace App\Culture\Entity;

use App\Culture\Repository\PostRepository;
use App\Security\Entity\User;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PostRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Post
{
    private const string LINK_PART_EMBED = '/embed/',
        LINK_PART_WATCH = '/watch?v=';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'posts')]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id', nullable: false)]
    private ?User $user;

    #[ORM\Column(type: 'datetime_immutable', options: ['default' => 'CURRENT_TIMESTAMP'])]
    private ?\DateTimeImmutable $inserted;

    #[ORM\Column(type: 'string')]
    private ?string $link;

    #[ORM\Column(type: 'boolean')]
    private bool $isActive;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $name;

    #[ORM\Column(type: 'boolean')]
    private bool $isPromoted;

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setUser(?User $user): void
    {
        $this->user = $user;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setInserted(?\DateTimeImmutable $inserted): void
    {
        $this->inserted = $inserted;
    }

    public function getInserted(): ?\DateTimeImmutable
    {
        return $this->inserted;
    }

    public function setLink(?string $link): void
    {
        $this->link = $link;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setIsPromoted(bool $isPromoted): void
    {
        $this->isPromoted = $isPromoted;
    }

    public function isPromoted(): bool
    {
        return $this->isPromoted;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist(): void
    {
        if (!$this->inserted) {
            $this->inserted = new \DateTimeImmutable();
        }
        $this->fixYoutubeLink();
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate(): void
    {
        $this->fixYoutubeLink();
    }

    /**
     * Fix "https://www.youtube.com/watch?v=U2bezBlEsRw" -> "https://www.youtube.com/embed/U2bezBlEsRw"
     */
    private function fixYoutubeLink(): void
    {
        if ($this->containsLinkPart(self::LINK_PART_EMBED)) {
            return;
        } elseif ($this->containsLinkPart(self::LINK_PART_WATCH)) {
            $videoId = mb_substr(
                $this->link,
                mb_strpos($this->link, self::LINK_PART_WATCH) + mb_strlen(self::LINK_PART_WATCH)
            );
            $this->link = "https://www.youtube.com/embed/{$videoId}";
        } else {
            $this->link = "https://www.youtube.com/embed/{$this->link}";
        }
    }

    private function containsLinkPart(string $linkPart): bool
    {
        return mb_strpos($this->link, $linkPart) !== false;
    }
}
