<?php

declare(strict_types=1);

namespace App\Culture\Service;

use App\Culture\Entity\Post;
use App\Culture\Repository\PostRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

readonly class PostService
{
    public function __construct(
        private PostRepository $postRepository
    ) {
    }

    /**
     * @return array<Post>
     */
    public function getAll(bool $isActive = true): array
    {
        return $this->postRepository->findBy(['isActive' => $isActive]);
    }

    /**
     * @return array<Post>
     */
    public function getAllPromoted(): array
    {
        return $this->postRepository->findBy(['isPromoted' => true]);
    }

    /**
     * @return Paginator<Post>
     */
    public function getAllPaged(int $page = 1, int $pageSize = 4): Paginator
    {
        return $this->postRepository->findPaged($page, $pageSize);
    }

    public function getById(int $id): ?Post
    {
        return $this->postRepository->findOneBy(['id' => $id]);
    }

    public function save(Post $post): void
    {
        $this->postRepository->save($post);
    }
}
