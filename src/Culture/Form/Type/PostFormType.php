<?php

declare(strict_types=1);

namespace App\Culture\Form\Type;

use App\Culture\Entity\Post;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class PostFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Název',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Prosím vyplňte název příspěvku',
                    ]),
                ],
            ])
            ->add('link', TextType::class, [
                'label' => 'Odkaz',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Prosím vyplňte odkaz',
                    ]),
                ],
            ])
            ->add('isActive', CheckboxType::class, [
                'label' => 'Aktivní',
                'required' => false
            ])
            ->add('isPromoted', CheckboxType::class, [
                'label' => 'Promovaný',
                'required' => false
            ])
            ->add('save', SubmitType::class, ['label' => 'Uložit'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
        ]);
    }
}
