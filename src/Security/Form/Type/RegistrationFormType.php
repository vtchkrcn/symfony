<?php

declare(strict_types=1);

namespace App\Security\Form\Type;

use App\Security\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('plainPassword', PasswordType::class, [
                'mapped' => false,
                'label' => 'Heslo',
                'attr' => ['autocomplete' => 'new-password'],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Prosím vyplňte heslo',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Heslo musí mít alespoň {{ limit }} znaků',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
            ->add('registrationSecret', PasswordType::class, [
                'mapped' => false,
                'label' => 'Registrační fráze',
                'attr' => ['autocomplete' => 'off'],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Prosím vyplňte tajnou registrační frázi',
                    ])
                ],
            ])
            ->add('save', SubmitType::class, ['label' => 'Registrovat'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
