<?php

declare(strict_types=1);

namespace App\Security\Service;

use App\Security\Entity\User;
use App\Security\Exception\InvalidRegistrationSecretException;
use App\Security\Repository\UserRepository;
use App\Security\Util\EmailVerifier;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class RegistrationService
{
    private UserRepository $userRepository;

    private EmailVerifier $emailVerifier;

    private UserPasswordHasherInterface $passwordHasher;

    private ?string $registrationSecret;

    public function __construct(
        UserRepository $userRepository,
        EmailVerifier $emailVerifier,
        UserPasswordHasherInterface $passwordHasher,
        ?string $registrationSecret
    ) {
        $this->userRepository = $userRepository;
        $this->emailVerifier = $emailVerifier;
        $this->passwordHasher = $passwordHasher;
        $this->registrationSecret = $registrationSecret;
    }

    /**
     * @throws InvalidRegistrationSecretException
     */
    public function register(User $user, string $plaintextPassword, string $registrationSecret): void
    {
        if ($this->registrationSecret != $registrationSecret) {
            throw new InvalidRegistrationSecretException('Registering user failed: Invalid registration secret');
        }

        $hashedPassword = $this->passwordHasher->hashPassword(
            $user,
            $plaintextPassword
        );
        $user->setPassword($hashedPassword);
        $this->userRepository->save($user);
        // Consider moving to facade
        $this->emailVerifier->sendEmailConfirmation(
            'registration_verify',
            $user,
            (new TemplatedEmail())
                ->from(new Address(\App\Util\Email\Address::INFO, 'Vtchkrcn Registrace'))
                ->to($user->getEmail())
                ->subject('Potvrzení registrace')
                ->htmlTemplate('registration/confirmation_email.html.twig')
        );
    }

    /**
     * @throws \SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface
     */
    public function verifyUser(string $uri, User $user): void
    {
        $this->emailVerifier->validateEmailConfirmation($uri, $user);
        $user->setIsVerified(true);
        $this->userRepository->save($user);
    }
}
