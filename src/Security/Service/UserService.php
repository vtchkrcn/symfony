<?php

declare(strict_types=1);

namespace App\Security\Service;

use App\Security\Entity\User;
use App\Security\Repository\UserRepository;

readonly class UserService
{
    public function __construct(
        private UserRepository $userRepository,
    ) {
    }

    /**
     * @return array<User>
     */
    public function getAll(): array
    {
        return $this->userRepository->findAll();
    }
}
