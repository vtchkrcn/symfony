<?php

declare(strict_types=1);

namespace App\Security\Entity;

use App\Culture\Entity\Post;
use App\Security\Repository\UserRepository;
use App\Test\Entity\Test;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[UniqueEntity(fields: ['email'], message: 'There is already an account with this email')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id;

    #[ORM\Column(type: 'string', length: 255, unique: true)]
    private ?string $email;

    /** @var array<int, string> $roles */
    #[ORM\Column(type: 'json')]
    private ?array $roles = [];

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $password;

    #[ORM\Column(type: 'boolean')]
    private bool $isVerified = false;

    /** @var PersistentCollection<int, Test> $results */
    #[ORM\OneToMany(targetEntity: Test::class, mappedBy: 'user')]
    private PersistentCollection $results;

    /** @var PersistentCollection<int, Post> $posts */
    #[ORM\OneToMany(targetEntity: Post::class, mappedBy: 'user')]
    private PersistentCollection $posts;

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): ?string
    {
        return (string) $this->email;
    }

    /**
     * @return array<string>
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @param array<string> $roles
     * @return $this
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    /**
     * @param PersistentCollection<int, Test> $results
     */
    public function setResults(PersistentCollection $results): void
    {
        $this->results = $results;
    }

    /**
     * @return PersistentCollection<int, Test>
     */
    public function getResults(): PersistentCollection
    {
        return $this->results;
    }

    /**
     * @param PersistentCollection<int, Post> $posts
     */
    public function setPosts(PersistentCollection $posts): void
    {
        $this->posts = $posts;
    }

    /**
     * @return PersistentCollection<int, Post>
     */
    public function getPosts(): PersistentCollection
    {
        return $this->posts;
    }
}
