<?php

declare(strict_types=1);

namespace App\Util\Generic;

use ReflectionClass;

/**
 * @deprecated Use build in enum instead
 */
class BaseEnum
{
    /**
     * @return array<int|string|float|bool>
     */
    public static function getAll(): array
    {
        $thisClass = new ReflectionClass(static::class);
        return $thisClass->getConstants();
    }

    /**
     * @param int|string|float|bool $value
     * @return bool
     */
    public static function contains($value): bool
    {
        return in_array($value, static::getAll());
    }
}
