<?php

declare(strict_types=1);

namespace App\Util\DateTime\Enum;

class DateTimeFormat
{
    public const CZECH_DATE = 'j.n.Y',
        CZECH_DATE_TIME_SECONDS = 'j.n.Y H:i:s',
        CZECH_DATE_TIME_MINUTES = 'j.n.Y H:i';
}
