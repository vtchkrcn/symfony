<?php

declare(strict_types=1);

namespace App\Util\Email;

use App\Util\Generic\BaseEnum;

class Address extends BaseEnum
{
    public const string INFO = 'info@vtchkrcn.cz';
}
