<?php

declare(strict_types=1);

namespace App\Util\Message;

use App\Util\Generic\BaseEnum;

class Flash extends BaseEnum
{
    public const string SUCCESS = 'success',
        DANGER = 'danger',
        WARNING = 'warning';
}
