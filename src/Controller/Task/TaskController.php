<?php

declare(strict_types=1);

namespace App\Controller\Task;

use App\Task\Entity\Task;
use App\Task\Form\Type\TaskFormType;
use App\Task\Service\TaskService;
use App\Util\Message\Flash;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class TaskController extends AbstractController
{
    private TaskService $taskService;


    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    #[Route('/tasks', name: 'tasks')]
    public function showTests(): Response
    {
        return $this->render('task/tasks.html.twig', [
            'tasks' => $this->taskService->getAll()
        ]);
    }

    #[IsGranted('ROLE_USER')]
    #[Route('/task/edit/{id?}', name: 'task_edit')]
    public function edit(Request $request): Response
    {
        $id = $request->get('id');
        $task = new Task();
        if ($id) {
            $task = $this->taskService->getById((int)$id);
        }
        $form = $this->createForm(TaskFormType::class, $task);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();
            $this->taskService->save($task);
            $this->addFlash(Flash::SUCCESS, 'Úkol byl uložen');
            return $this->redirectToRoute('tasks');
        }
        return $this->render('task/task.html.twig', [
            'task' => $task,
            'form' => $form->createView()
        ]);
    }
}
