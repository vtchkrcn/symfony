<?php

declare(strict_types=1);

namespace App\Controller\Culture;

use App\Culture\Entity\Post;
use App\Culture\Form\Type\PostFormType;
use App\Culture\Service\PostService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class CultureController extends AbstractController
{
    public function __construct(
        private PostService $postService
    ) {
    }

    #[Route('/culture', name: 'culture')]
    public function showPromotedPosts(): Response
    {
        return $this->render('culture/culture.html.twig', [
            'posts' => $this->postService->getAllPromoted(),
        ]);
    }

    #[Route('/culture/paged/{page?}', name: 'culture_paged')]
    public function showPagedPosts(?int $page, int $pageSize = 4): Response
    {
        $page = ($page > 0 ? $page : 1);
        $pageSize = ($pageSize > 0 ? $pageSize : 4);
        return $this->render('culture/paged.html.twig', [
            'pager' => $this->postService->getAllPaged($page, $pageSize),
            'page' => $page,
            'pageSize' => $pageSize,
        ]);
    }

    #[IsGranted('ROLE_USER')]
    #[Route('/test/start/{id}', name: 'test_start')]
    public function showInactivePosts(): Response
    {
        return $this->render('culture/culture.html.twig', [
            'posts' => $this->postService->getAll(false),
        ]);
    }

    #[IsGranted('ROLE_USER')]
    #[Route('/test/start/{id}', name: 'test_start')]
    public function editPost(?int $id, Request $request): Response
    {
        $post = new Post();
        if ($id) {
            $post = $this->postService->getById($id);
        }

        $form = $this->createForm(PostFormType::class, $post);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();
            $post->setUser($this->getUser());
            $this->postService->save($post);
            return $this->redirectToRoute('culture');
        }
        return $this->render('culture/post/edit.html.twig', [
            'form' => $form->createView(),
            'post' => $post,
        ]);
    }
}
