<?php

declare(strict_types=1);

namespace App\Controller\Test;

use App\Test\Form\Type\RunQuestionFormType;
use App\Test\Service\RunService;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class RunController extends AbstractController
{
    private RunService $runService;


    public function __construct(RunService $runService)
    {
        $this->runService = $runService;
    }

    #[IsGranted('ROLE_USER')]
    #[Route('/test/start/{id}', name: 'test_start')]
    public function start(int $id): Response
    {
        $result = null;
        try {
            $result = $this->runService->runTest($this->getUser(), $id);
        } catch (EntityNotFoundException $exception) {
            throw new NotFoundHttpException('Spuštění testu selhalo: Test nebyl nalezen');
        }
        return $this->redirectToRoute('test_run', ['resultId' => $result->getId()]);
    }

    #[IsGranted('ROLE_USER')]
    #[Route('/test/run/{resultId}', name: 'test_run')]
    public function showQuestion(int $resultId): Response
    {
        $result = $this->runService->getResultWithExistsCheck($resultId);
        $resultAnswer = $this->runService->getCurrentQuestionForResult($result);
        if (!$resultAnswer) {
            return $this->redirectToRoute('test_result_show', ['resultId' => $resultId]);
        }
        $form = $this->createForm(RunQuestionFormType::class, null, [
            'answers' => $resultAnswer->getQuestion()->getAnswers()
        ]);
        // Add total question count
        return $this->render(
            'test/run/question.html.twig',
            ['resultAnswer' => $resultAnswer, 'form' => $form->createView()]
        );
    }

    #[IsGranted('ROLE_USER')]
    #[Route('/test/run/submit/{resultId}', name: 'test_run_submit')]
    public function saveAnswer(int $resultId, Request $request): Response
    {
        $resultAnswer = $this->runService->saveUserAnswer($resultId, $request->get('selectedAnswerId'));
        $response = [];
        foreach ($resultAnswer->getQuestion()->getAnswers() as $answer) {
            $response['answers'][] = ['id' => $answer->getId(), 'isCorrect' => $answer->isCorrect()];
        }
        return $this->json($response);
    }

    #[IsGranted('ROLE_USER')]
    #[Route('/test/result/{resultId}', name: 'test_result_show')]
    public function showResult(int $resultId): Response
    {
        return $this->render('test/result/result.html.twig', [
            'result' => $this->runService->getResultWithExistsCheck($resultId)
        ]);
    }
}
