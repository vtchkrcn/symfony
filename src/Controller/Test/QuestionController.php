<?php

declare(strict_types=1);

namespace App\Controller\Test;

use App\Test\Entity\Answer;
use App\Test\Entity\Question;
use App\Test\Form\Type\AnswerFormType;
use App\Test\Form\Type\QuestionFormType;
use App\Test\Service\QuestionService;
use App\Util\Message\Flash;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class QuestionController extends AbstractController
{
    private QuestionService $questionService;

    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    #[IsGranted('ROLE_USER')]
    #[Route('/questions', name: 'questions')]
    public function showQuestions(): Response
    {
        return $this->render('test/question/questions.html.twig', [
            'questions' => $this->questionService->getAll()
        ]);
    }

    #[IsGranted('ROLE_USER')]
    #[Route('/question/show/{id}', name: 'question_show')]
    public function showQuestion(int $id): Response
    {
        return $this->render('test/question/show.html.twig', ['question' => $this->questionService->getById($id)]);
    }

    #[IsGranted('ROLE_USER')]
    #[Route('/question/edit/{id?}', name: 'question_edit')]
    public function editQuestion(Request $request): Response
    {
        $id = $request->get('id');
        $question = new Question();
        if ($id) {
            $question = $this->questionService->getById($id);
        }

        $form = $this->createForm(QuestionFormType::class, $question);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $question = $form->getData();
            $this->questionService->save($question);
            $this->addFlash(Flash::SUCCESS, 'Otázka byla uložena');
            return $this->redirectToRoute('question_show', ['id' => $question->getId()]);
        }
        return $this->render('test/question/edit.html.twig', [
            'question' => $question,
            'form' => $form->createView()
        ]);
    }

    #[IsGranted('ROLE_USER')]
    #[Route('/question/{questionId}/answer/{answerId?}', name: 'question_answer')]
    public function editAnswer(Request $request): Response
    {
        $id = $request->get('questionId');
        $question = $this->questionService->getById($id);
        $answerId = $request->get('answerId');
        $answer = new Answer();
        if ($answerId) {
            $answer = $this->questionService->getAnswerById($answerId);
        }
        $answer->setQuestion($question);

        $form = $this->createForm(AnswerFormType::class, $answer);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $answer = $form->getData();
            $this->questionService->saveAnswer($answer);
            $this->addFlash(Flash::SUCCESS, 'Odpověď byla uložena');
            return $this->redirectToRoute('question_show', ['id' => $question->getId()]);
        }
        return $this->render('test/question/answer/edit.html.twig', [
            'question' => $question,
            'answer' => $answer,
            'form' => $form->createView()
        ]);
    }
}
