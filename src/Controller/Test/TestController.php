<?php

declare(strict_types=1);

namespace App\Controller\Test;

use App\Test\Entity\Test;
use App\Test\Form\Type\TestFormType;
use App\Test\Form\Type\TestQuestionFormType;
use App\Test\Service\QuestionService;
use App\Test\Service\TestService;
use App\Util\Message\Flash;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class TestController extends AbstractController
{
    private TestService $testService;

    private QuestionService $questionService;

    public function __construct(TestService $testService, QuestionService $questionService)
    {
        $this->testService = $testService;
        $this->questionService = $questionService;
    }

    #[Route('/tests', name: 'tests')]
    public function showTests(): Response
    {
        return $this->render('test/tests.html.twig', [
            'tests' => $this->testService->getAll()
        ]);
    }

    #[IsGranted('ROLE_USER')]
    #[Route('/test/show/{id}', name: 'test_show')]
    public function show(int $id): Response
    {
        return $this->render('test/show.html.twig', ['test' =>  $this->testService->getById($id)]);
    }

    #[IsGranted('ROLE_USER')]
    #[Route('/test/edit/{id?}', name: 'test_edit')]
    public function edit(Request $request): Response
    {
        $id = $request->get('id');
        $test = new Test();
        if ($id) {
            $test = $this->testService->getById($id);
        }

        $form = $this->createForm(TestFormType::class, $test);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->testService->save($form->getData());
            $this->addFlash(Flash::SUCCESS, 'Test byl uložen');
            return $this->redirectToRoute('tests');
        }
        return $this->render('test/edit.html.twig', [
            'test' => $test,
            'form' => $form->createView()
        ]);
    }

    #[IsGranted('ROLE_USER')]
    #[Route('/test/questions/{id}', name: 'test_edit_question')]
    public function editQuestions(Request $request): Response
    {
        $id = $request->get('id');
        $test = $this->testService->getById($id);
        $form = $this->createForm(TestQuestionFormType::class, null, [
           'questions' => $this->questionService->getAll(),
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();
            $question = $formData['question'];
            $test->getQuestions()->add($question);
            $this->testService->save($test);
            $this->addFlash(Flash::SUCCESS, 'Otázka byla přidělena k testu');
            return $this->redirectToRoute('test_show', ['id' => $test->getId()]);
        }
        return $this->render('test/edit-question.html.twig', [
            'test' => $test,
            'form' => $form->createView(),
        ]);
    }
}
