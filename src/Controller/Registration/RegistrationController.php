<?php

declare(strict_types=1);

namespace App\Controller\Registration;

use App\Security\Entity\User;
use App\Security\Exception\InvalidRegistrationSecretException;
use App\Security\Form\Type\RegistrationFormType;
use App\Security\Repository\UserRepository;
use App\Security\Service\RegistrationService;
use App\Util\Message\Flash;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;

class RegistrationController extends AbstractController
{
    private RegistrationService $registrationService;

    public function __construct(RegistrationService $registrationService)
    {
        $this->registrationService = $registrationService;
    }

    #[Route('/registration', name: 'registration')]
    public function register(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->registrationService->register(
                    $user,
                    $form->get('plainPassword')->getData(),
                    $form->get('registrationSecret')->getData()
                );
                $this->addFlash(Flash::SUCCESS, 'Potvrzovací email byl odeslán na vaši emailovou adresu');
                return $this->redirectToRoute('index');
            } catch (InvalidRegistrationSecretException $exception) {
                $form->get('registrationSecret')
                    ->addError(new FormError('Neplatná registrační fráze'));
            }
        }

        return $this->render('registration/register.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/registration/verify', name: 'registration_verify')]
    public function verifyUserEmail(Request $request, UserRepository $userRepository): Response
    {
        $id = $request->get('id');
        $user = $userRepository->find($id);
        if (null === $user) {
            return $this->redirectToRoute('registration');
        }

        try {
            $this->registrationService->verifyUser($request->getUri(), $user);
        } catch (VerifyEmailExceptionInterface $exception) {
            $this->addFlash('verify_email_error', $exception->getReason());

            return $this->redirectToRoute('registration');
        }

        $this->addFlash(Flash::SUCCESS, 'Vaše emailová adresa byla potvrzena');
        return $this->redirectToRoute('index');
    }
}
