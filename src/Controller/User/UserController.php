<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Security\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class UserController extends AbstractController
{
    private UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    #[IsGranted('ROLE_USER')]
    #[Route('/users', name: 'users')]
    public function showUsers(): Response
    {
        return $this->render('user/users.html.twig', [
            'users' => $this->userService->getAll()
        ]);
    }
}
