<?php

declare(strict_types=1);

namespace App\Task\Service;

use App\Task\Entity\Task;
use App\Task\Repository\TaskRepository;

readonly class TaskService
{
    public function __construct(
        private TaskRepository $taskRepository
    ) {
    }

    /**
     * @return array<Task>
     */
    public function getAll(int $page = 1): array
    {
        return $this->taskRepository->findBy([], ['id' => 'DESC']);
    }

    public function getById(int $id): ?Task
    {
        return $this->taskRepository->findOneBy(['id' => $id]);
    }

    public function save(Task $task): void
    {
        $this->taskRepository->save($task);
    }
}
