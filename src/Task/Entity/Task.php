<?php

declare(strict_types=1);

namespace App\Task\Entity;

use App\Task\Repository\TaskRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TaskRepository::class)]
class Task
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $task;

    #[ORM\Column(type: 'datetime_immutable')]
    private ?\DateTimeImmutable $dueDate;

    #[ORM\Column(type: 'boolean')]
    private bool $isDone;

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setTask(?string $task): void
    {
        $this->task = $task;
    }

    public function getTask(): ?string
    {
        return $this->task;
    }

    public function setDueDate(?\DateTimeImmutable $dueDate): void
    {
        $this->dueDate = $dueDate;
    }

    public function getDueDate(): ?\DateTimeImmutable
    {
        return $this->dueDate;
    }

    public function setIsDone(bool $isDone): void
    {
        $this->isDone = $isDone;
    }

    public function isDone(): bool
    {
        return $this->isDone;
    }
}
