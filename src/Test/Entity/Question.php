<?php

declare(strict_types=1);

namespace App\Test\Entity;

use App\Test\Repository\QuestionRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

#[ORM\Entity(repositoryClass: QuestionRepository::class)]
class Question
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?string $id;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $question;

    #[ORM\Column(type: 'datetime_immutable', options: ['default' => 'CURRENT_TIMESTAMP'])]
    private ?\DateTimeImmutable $inserted;

    /** @var PersistentCollection<int, Test> $tests */
    #[ORM\ManyToMany(targetEntity: Test::class, mappedBy: 'questions')]
    private PersistentCollection $tests;

    /** @var PersistentCollection<int, Answer> $answers */
    #[ORM\OneToMany(targetEntity: Answer::class, mappedBy: 'question')]
    private PersistentCollection $answers;

    public function setId(?string $id): void
    {
        $this->id = $id;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setQuestion(?string $question): void
    {
        $this->question = $question;
    }

    public function getQuestion(): ?string
    {
        return $this->question;
    }

    public function setInserted(?\DateTimeImmutable $inserted): void
    {
        $this->inserted = $inserted;
    }

    public function getInserted(): ?\DateTimeImmutable
    {
        return $this->inserted;
    }

    /**
     * @param PersistentCollection<int, Test> $tests
     */
    public function setTests(PersistentCollection $tests): void
    {
        $this->tests = $tests;
    }

    /**
     * @return PersistentCollection<int, Test>
     */
    public function getTests(): PersistentCollection
    {
        return $this->tests;
    }

    /**
     * @param PersistentCollection<int, Answer> $answers
     */
    public function setAnswers(PersistentCollection $answers): void
    {
        $this->answers = $answers;
    }

    /**
     * @return PersistentCollection<int, Answer>
     */
    public function getAnswers(): PersistentCollection
    {
        return $this->answers;
    }
}
