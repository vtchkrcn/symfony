<?php

declare(strict_types=1);

namespace App\Test\Entity;

use App\Security\Entity\User;
use App\Test\Repository\ResultRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

#[ORM\Entity(repositoryClass: ResultRepository::class)]
class Result
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id;

    #[ORM\Column(type: 'datetime_immutable', options: ['default' => 'CURRENT_TIMESTAMP'])]
    private ?\DateTimeImmutable $inserted;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?\DateTimeImmutable $completed;

    #[ORM\ManyToOne(targetEntity: Test::class)]
    #[ORM\JoinColumn(name: 'test_id', referencedColumnName: 'id')]
    private ?Test $test;

    /** @var PersistentCollection<int, ResultAnswer> $answers */
    #[ORM\OneToMany(targetEntity: ResultAnswer::class, mappedBy: 'result')]
    private PersistentCollection $answers;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'results')]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id', nullable: false)]
    private ?User $user;

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setInserted(?\DateTimeImmutable $inserted): void
    {
        $this->inserted = $inserted;
    }

    public function getInserted(): ?\DateTimeImmutable
    {
        return $this->inserted;
    }

    public function setCompleted(?\DateTimeImmutable $completed): void
    {
        $this->completed = $completed;
    }

    public function getCompleted(): ?\DateTimeImmutable
    {
        return $this->completed;
    }

    public function setTest(?Test $test): void
    {
        $this->test = $test;
    }

    public function getTest(): ?Test
    {
        return $this->test;
    }

    /**
     * @param PersistentCollection<int, ResultAnswer> $answers
     */
    public function setAnswers(PersistentCollection $answers): void
    {
        $this->answers = $answers;
    }

    /**
     * @return PersistentCollection<int, ResultAnswer>
     */
    public function getAnswers(): PersistentCollection
    {
        return $this->answers;
    }

    public function setUser(?User $user): void
    {
        $this->user = $user;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }
}
