<?php

declare(strict_types=1);

namespace App\Test\Entity;

use App\Test\Repository\TestRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

#[ORM\Entity(repositoryClass: TestRepository::class)]
class Test
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $name;

    /** @var PersistentCollection<int, Question> $questions */
    #[ORM\ManyToMany(targetEntity: Question::class, inversedBy: 'tests')]
    #[ORM\JoinTable(name: 'test_question')]
    private PersistentCollection $questions;

    #[ORM\Column(type: 'datetime_immutable', options: ['default' => 'CURRENT_TIMESTAMP'])]
    private ?\DateTimeImmutable $inserted;

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param PersistentCollection<int, Question> $questions
     */
    public function setQuestions(PersistentCollection $questions): void
    {
        $this->questions = $questions;
    }

    /**
     * @return PersistentCollection<int, Question>
     */
    public function getQuestions(): PersistentCollection
    {
        return $this->questions;
    }

    public function setInserted(?\DateTimeImmutable $inserted): void
    {
        $this->inserted = $inserted;
    }

    public function getInserted(): ?\DateTimeImmutable
    {
        return $this->inserted;
    }
}
