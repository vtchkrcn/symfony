<?php

declare(strict_types=1);

namespace App\Test\Entity;

use App\Test\Repository\ResultAnswerRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ResultAnswerRepository::class)]
class ResultAnswer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id;

    #[ORM\ManyToOne(targetEntity: Result::class, inversedBy: 'answers')]
    #[ORM\JoinColumn(name: 'result_id', referencedColumnName: 'id')]
    private ?Result $result;

    #[ORM\ManyToOne(targetEntity: Question::class, inversedBy: 'resultAnswers')]
    #[ORM\JoinColumn(name: 'question_id', referencedColumnName: 'id')]
    private ?Question $question;

    #[ORM\ManyToOne(targetEntity: Answer::class)]
    #[ORM\JoinColumn(name: 'answer_id', referencedColumnName: 'id', nullable: true)]
    private ?Answer $answer;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private bool $isCorrect;

    #[ORM\Column(type: 'datetime_immutable')]
    private ?\DateTimeImmutable $updated;

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setResult(?Result $result): void
    {
        $this->result = $result;
    }

    public function getResult(): ?Result
    {
        return $this->result;
    }

    public function setQuestion(?Question $question): void
    {
        $this->question = $question;
    }

    public function getQuestion(): ?Question
    {
        return $this->question;
    }

    public function setAnswer(?Answer $answer): void
    {
        $this->answer = $answer;
    }

    public function getAnswer(): ?Answer
    {
        return $this->answer;
    }

    public function setIsCorrect(bool $isCorrect): void
    {
        $this->isCorrect = $isCorrect;
    }

    public function isCorrect(): bool
    {
        return $this->isCorrect;
    }

    public function setUpdated(?\DateTimeImmutable $updated): void
    {
        $this->updated = $updated;
    }

    public function getUpdated(): ?\DateTimeImmutable
    {
        return $this->updated;
    }

    /**
     * @ORM\PreUpdate
     */
    public function setModifiedOn(): void
    {
        $this->updated = new \DateTimeImmutable();
    }

    public function evaluateUserAnswer(int $selectedAnswerId): bool
    {
        foreach ($this->getQuestion()->getAnswers() as $answer) {
            if ($answer->getId() === $selectedAnswerId) {
                $this->setAnswer($answer);
                $this->setIsCorrect($answer->isCorrect());
            }
        }
        return $this->isCorrect;
    }
}
