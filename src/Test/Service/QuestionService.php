<?php

declare(strict_types=1);

namespace App\Test\Service;

use App\Test\Entity\Answer;
use App\Test\Entity\Question;
use App\Test\Repository\AnswerRepository;
use App\Test\Repository\QuestionRepository;

readonly class QuestionService
{
    public function __construct(
        private QuestionRepository $questionRepository,
        private AnswerRepository $answerRepository
    ) {
    }

    /**
     * @return Question[]
     */
    public function getAll(): array
    {
        return $this->questionRepository->findAll();
    }

    public function getById(int $id): ?Question
    {
        return $this->questionRepository->findOneBy(['id' => $id]);
    }

    public function save(Question $question): void
    {
        if (!$question->getId()) {
            $question->setInserted(new \DateTimeImmutable());
        }
        $this->questionRepository->save($question);
    }

    public function saveAnswer(Answer $answer): void
    {
        $this->answerRepository->save($answer);
    }

    public function getAnswerById(int $id): ?Answer
    {
        return $this->answerRepository->findOneBy(['id' => $id]);
    }

    /**
     * @param int|null $id
     * @return Answer[]
     */
    public function getAllAnswersByQuestionId(?int $id): array
    {
        return $this->answerRepository->findBy(['question' => $id]);
    }
}
