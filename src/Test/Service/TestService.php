<?php

declare(strict_types=1);

namespace App\Test\Service;

use App\Test\Entity\Test;
use App\Test\Repository\TestRepository;

readonly class TestService
{
    public function __construct(
        private TestRepository $testRepository
    ) {
    }

    /**
     * @return Test[]
     */
    public function getAll(): array
    {
        return $this->testRepository->findAll();
    }

    public function getById(int $id): ?Test
    {
        return $this->testRepository->findOneBy(['id' => $id]);
    }

    public function save(Test $test): void
    {
        if (!$test->getId()) {
            $test->setInserted(new \DateTimeImmutable());
        }
        $this->testRepository->save($test);
    }
}
