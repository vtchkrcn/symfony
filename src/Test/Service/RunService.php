<?php

declare(strict_types=1);

namespace App\Test\Service;

use App\Security\Entity\User;
use App\Test\Entity\Result;
use App\Test\Entity\ResultAnswer;
use App\Test\Repository\ResultAnswerRepository;
use App\Test\Repository\ResultRepository;
use App\Test\Repository\TestRepository;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;

use function PHPUnit\Framework\assertInstanceOf;

readonly class RunService
{
    public function __construct(
        private ResultRepository $resultRepository,
        private ResultAnswerRepository $resultAnswerRepository,
        private TestRepository $testRepository
    ) {
    }

    /**
     * @throws EntityNotFoundException
     */
    public function runTest(UserInterface $user, int $testId): Result
    {
        $test = $this->testRepository->findOneBy(['id' => $testId]);
        if (empty($test)) {
            throw new EntityNotFoundException('Running test failed: Test was not found');
        }
        $result = new Result();
        $result->setTest($test);
        $result->setInserted(new \DateTimeImmutable());
//        assertInstanceOf(User::class, $user);
        if (!$user instanceof User) {
            throw new \InvalidArgumentException('Invalid user type');
        }
        $result->setUser($user);
        $this->resultRepository->save($result);
        foreach ($test->getQuestions() as $question) {
            $answer = new ResultAnswer();
            $answer->setResult($result);
            $answer->setQuestion($question);
            $answer->setUpdated(new \DateTimeImmutable());
            $this->resultAnswerRepository->save($answer);
        }
        return $result;
    }

    public function getById(int $id): ?Result
    {
        return $this->resultRepository->findOneBy(['id' => $id]);
    }

    /**
     * @throws EntityNotFoundException
     */
    public function getResultWithExistsCheck(int $resultId): Result
    {
        $result = $this->getById($resultId);
        if (!$result) {
            throw new EntityNotFoundException('Loading result failed: Result not found');
        }
        return $result;
    }

    /**
     * @throws EntityNotFoundException
     */
    public function saveUserAnswer(int $resultId, int $selectedAnswerId): ResultAnswer
    {
        $result = $this->getResultWithExistsCheck($resultId);
        $resultAnswer = $this->getCurrentQuestionForResult($result);
        $resultAnswer->evaluateUserAnswer($selectedAnswerId);
        $this->updateResultAnswer($resultAnswer);
        return $resultAnswer;
    }

    public function getCurrentQuestionForResult(Result $result): ?ResultAnswer
    {
        return $this->resultAnswerRepository->findOneBy(
            ['result' => $result, 'answer' => null],
            ['id' => 'ASC']
        );
    }

    public function updateResultAnswer(ResultAnswer $answer): void
    {
        $this->resultAnswerRepository->save($answer);
    }
}
