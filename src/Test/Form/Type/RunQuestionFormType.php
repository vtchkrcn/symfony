<?php

declare(strict_types=1);

namespace App\Test\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RunQuestionFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('answer', ChoiceType::class, [
            'choices' => $options['answers'],
            'label' => false,
            'expanded' => true,
            'choice_value' => 'id',
            'choice_label' => 'content',
            'choice_attr' => function ($choice, $key, $value) {
                return [
                    'class' => 'inline-answer',
                    'data-answer-id' => $value
                ];
            },
        ])->add('submit', SubmitType::class, [
            'label' => 'Pokračovat'
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'answers' => [],
            'data_class' => null,
        ]);
    }
}
