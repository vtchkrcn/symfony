<?php

declare(strict_types=1);

namespace App\Test\Repository;

use App\Test\Entity\ResultAnswer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ResultAnswer>
 * @method ResultAnswer|null find($id, $lockMode = null, $lockVersion = null)
 * @method ResultAnswer|null findOneBy(array $criteria, array $orderBy = null)
 * @method array<ResultAnswer> findAll()
 * @method array<ResultAnswer> findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ResultAnswerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ResultAnswer::class);
    }

    public function save(ResultAnswer $resultAnswer): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($resultAnswer);
        $entityManager->flush();
    }
}
