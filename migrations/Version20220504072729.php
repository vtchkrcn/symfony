<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220504072729 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE result (id INT AUTO_INCREMENT NOT NULL, test_id INT DEFAULT NULL, inserted DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, completed DATETIME DEFAULT NULL, INDEX IDX_136AC1131E5D0459 (test_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE result_answer (id INT AUTO_INCREMENT NOT NULL, result_id INT DEFAULT NULL, question_id INT DEFAULT NULL, answer_id INT DEFAULT NULL, is_correct TINYINT(1) DEFAULT NULL, updated DATETIME NOT NULL, INDEX IDX_3E4271797A7B643 (result_id), INDEX IDX_3E4271791E27F6BF (question_id), INDEX IDX_3E427179AA334807 (answer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE result ADD CONSTRAINT FK_136AC1131E5D0459 FOREIGN KEY (test_id) REFERENCES test (id)');
        $this->addSql('ALTER TABLE result_answer ADD CONSTRAINT FK_3E4271797A7B643 FOREIGN KEY (result_id) REFERENCES result (id)');
        $this->addSql('ALTER TABLE result_answer ADD CONSTRAINT FK_3E4271791E27F6BF FOREIGN KEY (question_id) REFERENCES question (id)');
        $this->addSql('ALTER TABLE result_answer ADD CONSTRAINT FK_3E427179AA334807 FOREIGN KEY (answer_id) REFERENCES answer (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE result_answer DROP FOREIGN KEY FK_3E4271797A7B643');
        $this->addSql('DROP TABLE result');
        $this->addSql('DROP TABLE result_answer');
    }
}
