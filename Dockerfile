FROM php:8.3.8-apache

COPY ./config/docker/apache/000-default.conf /etc/apache2/sites-available/000-default.conf
COPY ./config/docker/php/php.ini /usr/local/etc/php/php.ini

WORKDIR /var/www/html

RUN apt-get update -y \
    && apt-get install -y \
    git \
    gnupg2 \
    libpng-dev \
    libzip-dev \
    && apt-get remove --purge -y javascript-common \
    && docker-php-ext-install \
    mysqli \
    pdo \
    pdo_mysql \
    calendar \
    gd \
    zip \
    && docker-php-ext-enable pdo_mysql \
    && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && pecl install xdebug \
    && docker-php-ext-enable xdebug \
    && curl -fsSL https://deb.nodesource.com/setup_16.x | bash \
    && apt-get update -y \
    && apt-get install -y nodejs \
    && npm install -g yarn \
    && a2enmod rewrite \
    && service apache2 restart